import configparser
import sys
from os.path import abspath, dirname, join

if hasattr(sys, '_MEIPASS'):
    curdir = sys._MEIPASS
else:
    curdir = join(abspath(dirname(__file__)))

botconfig = configparser.ConfigParser()
botconfig.read(join(curdir, "config.ini"))

VERSION_MAJOR = botconfig.get("Config", "VERSION_MAJOR")
VERSION_MINOR = botconfig.get("Config", "VERSION_MINOR")
VERSION_PATCH = botconfig.get("Config", "VERSION_PATCH")
VERSION_BUILD = botconfig.get("Config", "VERSION_BUILD")

__version_info__ = (VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH)
__version__ = ".".join(map(str, __version_info__))

__version_info_full__ = (VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_BUILD)
__version_full__ = ".".join(map(str, __version_info_full__))

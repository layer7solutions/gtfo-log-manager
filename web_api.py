import configparser
import sys
from os.path import abspath, dirname, join

import requests


class WebAPI(object):
    def __init__(self):
        # Get location of the image files
        # First tries to use the _MEIPASS for the PyInstaller onefile method
        # otherwise uses the location of the current script file
        if hasattr(sys, '_MEIPASS'):
            curdir = sys._MEIPASS
        else:
            curdir = join(abspath(dirname(__file__)))
        botconfig_location = join(curdir, "config.ini")
        # Now loads the config file
        botconfig = configparser.ConfigParser()
        botconfig.read(botconfig_location)
        self.files_api_url = botconfig.get("Config", "APIURL")

    def make_request(self, filepath, filename):
        try:
            fileobj = open(f"{filepath}\\{filename}", "rb")
            response = requests.post(
                self.files_api_url, files={"myFile": (f"{filename}", fileobj)}
            )
            return response
        except Exception as err:
            return None
